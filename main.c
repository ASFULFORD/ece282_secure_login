#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#define  oops(s,x) { perror(s); exit(x); }

void get_username(char *a);
void get_password(char *a);
void set_echo(int ac, char av);
int main()
{
	char *correct_username = "Anthony";
	char *correct_password = "password";
	char username[20];
	char password[20];
	
	get_username(username);
	while(1)
	{
		if(strcmp(username, correct_username) == 0)
			break;
		else
		{
			printf("Wrong!! Enter the correct username.\n");
			get_username(username);
		}
	}

	get_password(password);
	while(1)
	{
		if(strcmp(password, correct_password) == 0)
			break;
		else
		{
			printf("Wrong!! Enter the correct password.\n");
			get_password(password);
		}
	}
	printf("Verification Complete!\n");
}
void get_username(char *a)
{
	while(1)
	{
		printf("Username: ");
		gets(a);
		
		if(strlen(a) < 5)
		{
			printf("Username too short.\n");
			printf("You're username is %d characters long.\n", strlen(a));
			printf("Please enter a valid username.\n");
		}
		else
		{
			break;
		}
	}
}
void get_password(char *a)
{
	char y = 'y';
	char n = 'n';
	while(1)
	{
		printf("Password: ");
		set_echo(2, n);
		gets(a);
		set_echo(2, y);
		if(strlen(a) < 5)
		{
			printf("Password too short.\n");
			printf("You're password is %d characters long.\n", strlen(a));
			printf("Please enter a valid password.\n");
		}
		else
		{
			break;
		}
	}
}
void set_echo(int ac, char av)
{
	struct termios info;

    if ( ac == 1 ) 
		exit(0);

    if ( tcgetattr(0,&info) == -1 )          /* get attribs   */
		oops("tcgettattr", 1);

    if ( av == 'y' )
        info.c_lflag |= ECHO ;          /* turn on bit    */
    else
        info.c_lflag &= ~ECHO ;         /* turn off bit   */

    if ( tcsetattr(0,TCSANOW,&info) == -1 ) /* set attribs    */
        oops("tcsetattr",2);
}
